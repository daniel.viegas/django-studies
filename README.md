# Django-Studies

O objetivo desse repositório é documentar todos os avanços realizados ao estudar o Framework de Web Django. Escrito em Python, esse Framework tem sido muito utilizado em diversas aplicações e foi escolhido como o primeiro Framework que irei trabalhar devido a minha proximidade com Python ser maior nesse momento.

Dentre as características do Django estão a velocidade para se criar uma aplicação, a escalabilidade para receber mais visitas conforme necessário e a segurança de ajudar o desenvolvedor a se proteger de práticas como SQL injection, clickjacking e cross-site scripting. 

O objetivo do projeto é mostrar essas características conforme foram aprendidas e conforme novas coisas forem mostradas. Para projetos completos de ciência de dados usando esse Framework juntamente a outras ferramentas serão criados projetos a parte.
